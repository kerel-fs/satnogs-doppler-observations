# SatNOGS Doppler Observations

Share doppler measurements extracted from SatNOGS Observations. To be used in conjunction with STRF.


## Note
The Site-ID is equal to the SatNOGS Station ID.

## Analyses

### 2022-01-17-01-kerel-kraksat
- Input data
  ```
  5311463.dat
  5311480.dat
  5311481.dat
  5311492.dat
  5311586.dat
  5311587.dat
  5311639.dat
  5312907.dat
  5312953.dat
  5313581.dat
  5313582.dat
  5313863.dat
  ```

- Input TLE
  ```
  ../data_shared/tles/5313863.txt
  ```

- Free parameters:
  MA, MM & B\*

- Result: see `analysis_sets/2022-01-17-01-kraksat.dat`
